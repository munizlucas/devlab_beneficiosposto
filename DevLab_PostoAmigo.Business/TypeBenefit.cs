﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevLab_PostoAmigo.Data;

namespace DevLab_PostoAmigo.Business
{
    public class TypeBenefit
    {
        public static dynamic List()
        {
            using (var context = new Data.PostoAmigoEntities())
            {
                return context.PostoAmigoTypeBenefit
                    .Select(b => new {
                        IdType = b.IdType,
                        Name = b.Name
                    })
                    .ToList();
            }
        }
        public static List<Return.TypeBenefit> ListType()
        {
            using (var context = new Data.PostoAmigoEntities())
            {
                return context.PostoAmigoTypeBenefit
                    .Select(b => new Return.TypeBenefit
                    {
                        IdType = b.IdType,
                        Name = b.Name
                    })
                    .ToList();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using DevLab_PostoAmigo.Data;

namespace DevLab_PostoAmigo.Business.Util
{
    public static class Geral
    {
        public static void CriaCookie(PostoAmigoAdminLogin login)
        {
            var cookie = new HttpCookie("Admin");

            cookie["IdAdmin"] = login.IdAdmin.ToString(CultureInfo.InvariantCulture);
            cookie["Name"] = HttpContext.Current.Server.UrlEncode(login.Name);
            cookie["Email"] = login.Email;

            var dtNow = DateTime.Now;
            var tsMinute = new TimeSpan(0, 0, 20, 0);
            cookie.Expires = dtNow + tsMinute;

            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public static PostoAmigoAdminLogin RecuperaCookieAdmin()
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies["Admin"];

                if (cookie == null)
                {
                    return new PostoAmigoAdminLogin();
                }
                else
                {
                    var admin = new PostoAmigoAdminLogin
                    {

                        IdAdmin = Convert.ToInt32(cookie.Value.Split('&')[0].Split('=')[1]),
                        Name = HttpContext.Current.Server.UrlDecode(cookie.Value.Split('&')[1].Split('=')[1]),
                        Email = cookie.Value.Split('&')[2].Split('=')[1]
                    };

                    return admin;
                }
            }
            catch { }
       
            return new PostoAmigoAdminLogin();
        }

        public static void RemoveCookie(string cookieName)
        {
            if (HttpContext.Current.Response.Cookies[cookieName] != null)
            {
                HttpContext.Current.Response.Cookies[cookieName].Value = null;
                HttpContext.Current.Response.Cookies[cookieName].Expires = DateTime.Now.AddMonths(-1);
            }
        }
    }
}

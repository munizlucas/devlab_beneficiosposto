﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevLab_PostoAmigo.Data;

namespace DevLab_PostoAmigo.Business
{
    public class StatusEvent
    {
        public static List<Return.StatusEvent> ListStatus()
        {
            using (var context = new Data.PostoAmigoEntities())
            {
                return context.PostoAmigoStatusEvent
                    .Select(b => new Return.StatusEvent
                    {
                        IdStatus = b.IdStatus,
                        Name = b.Name
                    })
                    .ToList();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevLab_PostoAmigo.Data;

namespace DevLab_PostoAmigo.Business
{
    public class AdminLogin
    {
        public static PostoAmigoAdminLogin Login(string email, string password)
        {
            try
            {
                using (var context = new Data.PostoAmigoEntities())
                {
                    var result = context.PostoAmigoAdminLogin.Where(b => b.Email == email &&
                                                                         b.Password == password &&
                                                                         b.Active == true).FirstOrDefault();

                    return result;
                }
            }
            catch 
            {
                return null;
            }

        }
    }
}

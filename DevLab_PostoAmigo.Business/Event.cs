﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevLab_PostoAmigo.Data;

namespace DevLab_PostoAmigo.Business
{
    public class Event
    {
        public static dynamic ListEvent(long Ibm)
        {
            using (var context = new Data.PostoAmigoEntities())
            {
                return context.PostoAmigoEvent
                    .Where(b => b.Ibm == Ibm && b.IdStatus != (int)Enum.StatusEvent.Cancel)
                    .Select(b => new
                    {
                        IdEvent = b.IdEvent,
                        Ibm = b.Ibm,
                        IdStatus = b.IdStatus,
                        Name = b.Name,
                        Description = b.Description,
                        DateEvent = b.DateEvent,
                        HourStart = b.HourStart,
                        HourEnd = b.HourEnd,
                        Comments = b.Comments,
                        Reason = b.Reason,
                        DateCreate = b.DateCreate,
                        DateUpdate = b.DateUpdate,
                        NameStatus = b.PostoAmigoStatusEvent.Name
                    })
                    .ToList();
            }
        }
        public static object GetEvent(int IdEvent)
        {
            using (var context = new Data.PostoAmigoEntities())
            {
                return context.PostoAmigoEvent
                    .Where(b => b.IdEvent == IdEvent)
                    .Select(b => new
                    {
                        IdEvent = b.IdEvent,
                        Ibm = b.Ibm,
                        IdStatus = b.IdStatus,
                        Name = b.Name,
                        Description = b.Description,
                        DateEvent = b.DateEvent,
                        HourStart = b.HourStart,
                        HourEnd = b.HourEnd,
                        Comments = b.Comments,
                        Reason = b.Reason,
                        DateCreate = b.DateCreate,
                        DateUpdate = b.DateUpdate,
                        NameStatus = b.PostoAmigoStatusEvent.Name
                    })
                    .FirstOrDefault();
            }
        }

        public static Return.Model Create(PostoAmigoEvent eevent)
        {
            try
            {
                using (var context = new Data.PostoAmigoEntities())
                {
                    eevent.IdStatus = (int)Enum.StatusEvent.Pending;
                    eevent.DateCreate = DateTime.Now;
                    context.PostoAmigoEvent.Add(eevent);
                    context.SaveChanges();

                    return new Return.Model(200, true, "Evento cadastrado com sucesso.");
                }
            }
            catch (Exception ex)
            {
                return new Return.Model(400, false, ex.Message.ToString());
            }

        }
        public static Return.Model Edit(PostoAmigoEvent eevent)
        {
            try
            {
                using (var context = new Data.PostoAmigoEntities())
                {
                    var result = context.PostoAmigoEvent.Where(b => b.IdEvent == eevent.IdEvent).FirstOrDefault();
                    if (result != null)
                    {
                        if (result.IdStatus == (int)Enum.StatusEvent.Disapproved)
                        {
                            result.Name = eevent.Name;
                            result.Description = eevent.Description;
                            result.HourStart = eevent.HourStart;
                            result.HourEnd = eevent.HourEnd;
                            result.Comments = eevent.Comments;
                            result.DateUpdate = DateTime.Now;
                            result.IdStatus = (int)Enum.StatusEvent.Pending;
                            context.Entry(result).State = System.Data.Entity.EntityState.Modified;

                            context.SaveChanges();

                            return new Return.Model(200, true, "Evento editado com sucesso.");
                        }
                        else
                        {
                            return new Return.Model(200, false, "Evento deve estar reprovado.");
                        }
                    }
                    else
                    {
                        return new Return.Model(200, false, "Evento não encontrado.");
                    }
                }
            }
            catch (Exception ex)
            {
                return new Return.Model(400, false, ex.Message.ToString());
            }

        }
        public static Return.Model Cancel(int IdEvent)
        {
            try
            {
                using (var context = new Data.PostoAmigoEntities())
                {
                    var result = context.PostoAmigoEvent.Where(b => b.IdEvent == IdEvent).FirstOrDefault();
                    if (result != null)
                    {
                        if (result.IdStatus == (int)Enum.StatusEvent.Approved)
                        {

                            result.DateUpdate = DateTime.Now;
                            result.IdStatus = (int)Enum.StatusEvent.Cancel;
                            context.Entry(result).State = System.Data.Entity.EntityState.Modified;
                            context.SaveChanges();

                            return new Return.Model(200, true, "Evento cancelado com sucesso.");
                        }
                        else
                        {
                            return new Return.Model(200, false, "Evento deve estar aprovado.");
                        }
                    }
                    else
                    {
                        return new Return.Model(200, false, "Evento não encontrado.");
                    }
                }
            }
            catch (Exception ex)
            {
                return new Return.Model(400, false, ex.Message.ToString());
            }
        }

        public static List<Return.Event> ListEvent(long? Ibm, int? IdStatus)
        {
            using (var context = new Data.PostoAmigoEntities())
            {
                return context.PostoAmigoEvent
                    .Where(b => (Ibm == null || b.Ibm == Ibm) &&
                                (IdStatus == null || b.IdStatus == IdStatus))
                    .Select(b => new Return.Event
                    {
                        IdEvent = b.IdEvent,
                        Ibm = b.Ibm,
                        IdStatus = b.IdStatus,
                        Name = b.Name,
                        Description = b.Description,
                        DateEvent = b.DateEvent,
                        HourStart = b.HourStart,
                        HourEnd = b.HourEnd,
                        Comments = b.Comments,
                        Reason = b.Reason,
                        DateCreate = b.DateCreate,
                        DateUpdate = b.DateUpdate,
                        NameStatus = b.PostoAmigoStatusEvent.Name
                    })
                    .ToList();
            }
        }
        public static Return.Event Details(int IdEvent)
        {
            using (var context = new Data.PostoAmigoEntities())
            {
                return context.PostoAmigoEvent
                    .Where(b => b.IdEvent == IdEvent)
                    .Select(b => new Return.Event
                    {
                        IdEvent = b.IdEvent,
                        Ibm = b.Ibm,
                        IdStatus = b.IdStatus,
                        Name = b.Name,
                        Description = b.Description,
                        DateEvent = b.DateEvent,
                        HourStart = b.HourStart,
                        HourEnd = b.HourEnd,
                        Comments = b.Comments,
                        DateCreate = b.DateCreate,
                        DateUpdate = b.DateUpdate,
                        Reason = b.Reason,
                        NameStatus = b.PostoAmigoStatusEvent.Name
                    })
                    .FirstOrDefault();
            }
        }
        public static void Edit(int IdEvent, int IdStatus, string Name, string Description, string Comments, string Reason)
        {
            try
            {
                using (var context = new Data.PostoAmigoEntities())
                {
                    var result = context.PostoAmigoEvent.Where(b => b.IdEvent == IdEvent).FirstOrDefault();
                    if (result != null)
                    {
                        result.Name = Name;
                        result.Description = Description;
                        result.Comments = Comments;
                        result.IdStatus = IdStatus;
                        result.Reason = Reason;
                        result.DateUpdate = DateTime.Now;

                        context.Entry(result).State = System.Data.Entity.EntityState.Modified;

                        context.SaveChanges();
                    }
                }
            }
            catch
            {
            }
        }
    }
}

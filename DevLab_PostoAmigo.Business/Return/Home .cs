﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLab_PostoAmigo.Business.Return
{
    public class Home
    {
        public int BenefitPending { get; set; }
        public int BenefitApproved { get; set; }
        public int BenefitDisapproved { get; set; }
        public int BenefitCancel { get; set; }

        public int EventPending { get; set; }
        public int EventApproved { get; set; }
        public int EventDisapproved { get; set; }
        public int EventCancel { get; set; }
    }
}

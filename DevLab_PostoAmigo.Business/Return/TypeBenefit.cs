﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLab_PostoAmigo.Business.Return
{
    public class TypeBenefit
    {
        public int IdType { get; set; }
        public string Name { get; set; }
    }
}

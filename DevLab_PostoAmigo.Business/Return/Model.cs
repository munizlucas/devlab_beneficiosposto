﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLab_PostoAmigo.Business.Return
{
    public class Model
    {
        public int StatusCode { get; set; }
        public bool Sucess { get; set; }
        public string Message { get; set; }

        public Model(int StatusCode, bool Sucess, string Message)
        {
            this.StatusCode = StatusCode;
            this.Sucess = Sucess;
            this.Message = Message;
        }
    }
}

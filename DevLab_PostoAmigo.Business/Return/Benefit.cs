﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLab_PostoAmigo.Business.Return
{
    public class Benefit
    {
        public int IdBenefit { get; set; }
        public long Ibm { get; set; }
        public int IdStatus { get; set; }
        public int IdType { get; set; }
        public string Description { get; set; }
        public string Comments { get; set; }
        public int Quantity { get; set; }
        public Nullable<System.DateTime> DateStart { get; set; }
        public Nullable<System.DateTime> DateEnd { get; set; }
        public string Reason { get; set; }
        public System.DateTime DateCreate { get; set; }
        public Nullable<System.DateTime> DateUpdate { get; set; }

        public string NameStatus { get; set; }
        public string NameType { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevLab_PostoAmigo.Data;

namespace DevLab_PostoAmigo.Business
{
    public class StatusBenefit
    {
        public static List<Return.StatusBenefit> ListStatus()
        {
            using (var context = new Data.PostoAmigoEntities())
            {
                return context.PostoAmigoStatusBenefit
                    .Select(b => new Return.StatusBenefit
                    {
                        IdStatus = b.IdStatus,
                        Name = b.Name
                    })
                    .ToList();
            }
        }
    }
}

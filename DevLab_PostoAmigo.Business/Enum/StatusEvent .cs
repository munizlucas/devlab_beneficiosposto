﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevLab_PostoAmigo.Business.Enum
{
    public enum StatusEvent
    {
        Pending = 1,
        Approved = 2,
        Disapproved = 3,
        Cancel = 4
    }
}

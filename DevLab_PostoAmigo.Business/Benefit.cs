﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevLab_PostoAmigo.Data;

namespace DevLab_PostoAmigo.Business
{
    public class Benfit
    {
        public static dynamic ListBenefit(long Ibm)
        {
            using (var context = new Data.PostoAmigoEntities())
            {
                return context.PostoAmigoBenefit
                    .Where(b => b.Ibm == Ibm && b.IdStatus != (int)Enum.StatusBenfit.Cancel)
                    .Select(b => new
                    {
                        IdBenefit = b.IdBenefit,
                        Ibm = b.Ibm,
                        IdStatus = b.IdStatus,
                        IdType = b.IdType,
                        Description = b.Description,
                        Comments = b.Comments,
                        Quantity = b.Quantity,
                        DateStart = b.DateStart,
                        DateEnd = b.DateEnd,
                        Reason = b.Reason,
                        DateCreate = b.DateCreate,
                        DateUpdate = b.DateUpdate,
                        NameStatus = b.PostoAmigoStatusBenefit.Name,
                        NameType = b.PostoAmigoTypeBenefit.Name
                    })
                    .ToList();
            }
        }
        public static object GetBenefit(int IdBenefit)
        {
            using (var context = new Data.PostoAmigoEntities())
            {
                return context.PostoAmigoBenefit
                    .Where(b => b.IdBenefit == IdBenefit)
                    .Select(b => new
                    {
                        IdBenefit = b.IdBenefit,
                        Ibm = b.Ibm,
                        IdStatus = b.IdStatus,
                        IdType = b.IdType,
                        Description = b.Description,
                        Comments = b.Comments,
                        Quantity = b.Quantity,
                        DateStart = b.DateStart,
                        DateEnd = b.DateEnd,
                        Reason = b.Reason,
                        DateCreate = b.DateCreate,
                        DateUpdate = b.DateUpdate,
                        NameStatus = b.PostoAmigoStatusBenefit.Name,
                        NameType = b.PostoAmigoTypeBenefit.Name
                    })
                    .FirstOrDefault();
            }
        }

        public static Return.Model Create(PostoAmigoBenefit benefit)
        {
            try
            {
                using (var context = new Data.PostoAmigoEntities())
                {
                    benefit.IdStatus = (int)Enum.StatusBenfit.Pending;
                    benefit.DateCreate = DateTime.Now;
                    context.PostoAmigoBenefit.Add(benefit);
                    context.SaveChanges();

                    return new Return.Model(200, true, "Benefício cadastrado com sucesso.");
                }
            }
            catch (Exception ex)
            {
                return new Return.Model(400, false, ex.Message.ToString());
            }

        }
        public static Return.Model Edit(int IdBenefit, string Description, DateTime? DateStart, DateTime? DateEnd)
        {
            if ((Description != null) || (DateStart != null) || (DateEnd != null))
            {
                try
                {
                    using (var context = new Data.PostoAmigoEntities())
                    {
                        var result = context.PostoAmigoBenefit.Where(b => b.IdBenefit == IdBenefit).FirstOrDefault();
                        if (result != null)
                        {
                            if (result.IdStatus == (int)Enum.StatusBenfit.Disapproved)
                            {
                                if (Description != null)
                                    result.Description = Description;

                                if (DateStart != null)
                                    result.DateStart = DateStart;

                                if (DateEnd != null)
                                    result.DateEnd = DateEnd;

                                result.DateUpdate = DateTime.Now;
                                result.IdStatus = (int)Enum.StatusBenfit.Pending;
                                context.Entry(result).State = System.Data.Entity.EntityState.Modified;

                                context.SaveChanges();

                                return new Return.Model(200, true, "Benefício editado com sucesso.");
                            }
                            else
                            {
                                return new Return.Model(200, false, "Benefício deve estar reprovado.");
                            }
                        }
                        else
                        {
                            return new Return.Model(200, false, "Benefício não encontrado.");
                        }
                    }
                }
                catch (Exception ex)
                {
                    return new Return.Model(400, false, ex.Message.ToString());
                }
            }
            else {
                return new Return.Model(400, false, "Descrição ou periodo devem ser informados.");
            }
        }
        public static Return.Model Cancel(int IdBenefit)
        {
            try
            {
                using (var context = new Data.PostoAmigoEntities())
                {
                    var result = context.PostoAmigoBenefit.Where(b => b.IdBenefit == IdBenefit).FirstOrDefault();
                    if (result != null)
                    {
                        if (result.IdStatus == (int)Enum.StatusBenfit.Approved)
                        {

                            result.DateUpdate = DateTime.Now;
                            result.IdStatus = (int)Enum.StatusBenfit.Cancel;
                            context.Entry(result).State = System.Data.Entity.EntityState.Modified;
                            context.SaveChanges();

                            return new Return.Model(200, true, "Benefício cancelado com sucesso.");
                        }
                        else
                        {
                            return new Return.Model(200, false, "Benefício deve estar aprovado.");
                        }
                    }
                    else
                    {
                        return new Return.Model(200, false, "Benefício não encontrado.");
                    }
                }
            }
            catch (Exception ex)
            {
                return new Return.Model(400, false, ex.Message.ToString());
            }
        }
        public static Return.Model Extend(int IdBenefit, DateTime? DateEnd)
        {
            try
            {
                using (var context = new Data.PostoAmigoEntities())
                {
                    var result = context.PostoAmigoBenefit.Where(b => b.IdBenefit == IdBenefit).FirstOrDefault();
                    if (result != null)
                    {
                        if (result.IdStatus == (int)Enum.StatusBenfit.Approved)
                        {
                            result.DateUpdate = DateTime.Now;
                            result.DateEnd = DateEnd;
                            context.Entry(result).State = System.Data.Entity.EntityState.Modified;
                            context.SaveChanges();

                            return new Return.Model(200, true, "Benefício prorrogado com sucesso.");
                        }
                        else
                        {
                            return new Return.Model(200, false, "Benefício deve estar aprovado.");
                        }
                    }
                    else
                    {
                        return new Return.Model(200, false, "Benefício não encontrado.");
                    }
                }
            }
            catch (Exception ex)
            {

                return new Return.Model(400, false, ex.Message.ToString());
            }
        }

        public static List<Return.Benefit> ListBenefit(long? Ibm, int? IdStatus, int? IdType)
        {
            using (var context = new Data.PostoAmigoEntities())
            {
                return context.PostoAmigoBenefit
                    .Where(b => (Ibm == null || b.Ibm == Ibm) &&
                                (IdStatus == null || b.IdStatus == IdStatus) &&
                                (IdType == null || b.IdType == IdType))
                    .Select(b => new Return.Benefit
                    {
                        IdBenefit = b.IdBenefit,
                        Ibm = b.Ibm,
                        IdStatus = b.IdStatus,
                        IdType = b.IdType,
                        Description = b.Description,
                        Comments = b.Comments,
                        Quantity = b.Quantity,
                        DateStart = b.DateStart,
                        DateEnd = b.DateEnd,
                        Reason = b.Reason,
                        DateCreate = b.DateCreate,
                        DateUpdate = b.DateUpdate,
                        NameStatus = b.PostoAmigoStatusBenefit.Name,
                        NameType = b.PostoAmigoTypeBenefit.Name
                    })
                    .ToList();
            }
        }
        public static Return.Benefit Details(int IdBenefit)
        {
            using (var context = new Data.PostoAmigoEntities())
            {
                return context.PostoAmigoBenefit
                    .Where(b => b.IdBenefit == IdBenefit)
                    .Select(b => new Return.Benefit
                    {
                        IdBenefit = b.IdBenefit,
                        Ibm = b.Ibm,
                        IdStatus = b.IdStatus,
                        IdType = b.IdType,
                        Description = b.Description,
                        Comments = b.Comments,
                        Quantity = b.Quantity,
                        DateStart = b.DateStart,
                        DateEnd = b.DateEnd,
                        Reason = b.Reason,
                        DateCreate = b.DateCreate,
                        DateUpdate = b.DateUpdate,
                        NameStatus = b.PostoAmigoStatusBenefit.Name,
                        NameType = b.PostoAmigoTypeBenefit.Name
                    })
                    .FirstOrDefault();
            }
        }
        public static void Edit(int IdBenefit, int IdStatus, string Description, string Comments, string Reason)
        {
            try
            {
                using (var context = new Data.PostoAmigoEntities())
                {
                    var result = context.PostoAmigoBenefit.Where(b => b.IdBenefit == IdBenefit).FirstOrDefault();
                    if (result != null)
                    {
                        result.Description = Description;
                        result.Comments = Comments;
                        result.IdStatus = IdStatus;
                        result.Reason = Reason;
                        result.DateUpdate = DateTime.Now;

                        context.Entry(result).State = System.Data.Entity.EntityState.Modified;

                        context.SaveChanges();
                    }
                }
            }
            catch
            {
            }
        }
    }
}

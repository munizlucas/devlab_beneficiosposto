﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevLab_PostoAmigo.Data;

namespace DevLab_PostoAmigo.Business
{
    public class Home
    {
        public static Return.Home Get()
        {
            try
            {
                using (var context = new Data.PostoAmigoEntities())
                {
                    Return.Home home = new Return.Home();

                    home.BenefitApproved = context.PostoAmigoBenefit.Where(b => b.IdStatus == (int)Enum.StatusBenfit.Approved).Count();
                    home.BenefitDisapproved = context.PostoAmigoBenefit.Where(b => b.IdStatus == (int)Enum.StatusBenfit.Disapproved).Count();
                    home.BenefitCancel = context.PostoAmigoBenefit.Where(b => b.IdStatus == (int)Enum.StatusBenfit.Cancel).Count();
                    home.BenefitPending = context.PostoAmigoBenefit.Where(b => b.IdStatus == (int)Enum.StatusBenfit.Pending).Count();

                    home.EventApproved = context.PostoAmigoEvent.Where(b => b.IdStatus == (int)Enum.StatusEvent.Approved).Count();
                    home.EventDisapproved = context.PostoAmigoEvent.Where(b => b.IdStatus == (int)Enum.StatusEvent.Disapproved).Count();
                    home.EventCancel = context.PostoAmigoEvent.Where(b => b.IdStatus == (int)Enum.StatusEvent.Cancel).Count();
                    home.EventPending = context.PostoAmigoEvent.Where(b => b.IdStatus == (int)Enum.StatusEvent.Pending).Count();

                    return home;
                }
            }
            catch 
            {
                return null;
            }
        } 
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevLab_PostoAmigo.Data;

namespace DevLab_PostoAmigo.Business
{
    public class BenfitParticipant
    {
        public static Return.Model Create(PostoAmigoBenefitParticipant participant)
        {
            try
            {
                using (var context = new Data.PostoAmigoEntities())
                {
                    participant.DateCreate = DateTime.Now;
                    context.PostoAmigoBenefitParticipant.Add(participant);
                    context.SaveChanges();

                    return new Return.Model(200, true, "Participante cadastrado com sucesso.");
                }
            }
            catch (Exception ex)
            {
                return new Return.Model(400, false, ex.Message.ToString());
            }

        }
        public static object GetQuantity(int IdBenefit)
        {
            object quantity;

            using (var context = new Data.PostoAmigoEntities())
            {
                quantity = context.PostoAmigoBenefitParticipant
                    .Where(b => b.IdBenefit == IdBenefit)
                    .Select(b => new
                    {
                        Id = b.IdBenefit,
                        Quantity = b.Quantity
                    })
                    .OrderByDescending(b => b.Id)
                    .FirstOrDefault();
            }

            if (quantity == null) {

                using (var context = new Data.PostoAmigoEntities())
                {
                    quantity = context.PostoAmigoBenefit
                        .Where(b => b.IdBenefit == IdBenefit)
                        .Select(b => new
                        {
                            Id = b.IdBenefit,
                            Quantity = b.Quantity
                        })
                        .FirstOrDefault();
                }

            }

            return quantity;
        }
    }
}

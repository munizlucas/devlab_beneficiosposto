//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DevLab_PostoAmigo.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class PostoAmigoStatusBenefit
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PostoAmigoStatusBenefit()
        {
            this.PostoAmigoBenefit = new HashSet<PostoAmigoBenefit>();
        }
    
        public int IdStatus { get; set; }
        public string Name { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PostoAmigoBenefit> PostoAmigoBenefit { get; set; }
    }
}

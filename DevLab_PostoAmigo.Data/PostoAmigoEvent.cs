//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DevLab_PostoAmigo.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class PostoAmigoEvent
    {
        public int IdEvent { get; set; }
        public long Ibm { get; set; }
        public int IdStatus { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public System.DateTime DateEvent { get; set; }
        public string HourStart { get; set; }
        public string HourEnd { get; set; }
        public string Comments { get; set; }
        public System.DateTime DateCreate { get; set; }
        public Nullable<System.DateTime> DateUpdate { get; set; }
        public string Reason { get; set; }
    
        public virtual PostoAmigoStatusEvent PostoAmigoStatusEvent { get; set; }
    }
}

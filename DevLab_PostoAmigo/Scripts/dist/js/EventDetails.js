﻿$(document).ready(function () {

    $("#dvReason").hide();
    $("#btnSalvar").hide();

    // STATUS
    $("#IdStatus").on("change", function () {
        var value = $(this).val();
        var dvReason = $("#dvReason");

        if (value == 3) /** Evento Reprovado **/ {
            dvReason.show();
        } else {
            dvReason.hide();
        }

        $("#btnSalvar").show();
    });

    $("#txtName").on("input", function () {
        $("#btnSalvar").show();
    });

    $("#txtDescription").on("input", function () {
        $("#btnSalvar").show();
    });

    $("#txtComments").on("input", function () {
        $("#btnSalvar").show();
    });

    // VOLTAR
    $("#btnVoltar").on("click", function (event) {
        event.preventDefault();
        window.location.href = "/Evento/";
        //window.location.href = "/beneficiodoposto/Evento/";
    });

    // SALVA
    $("#btnSalvar").click(function (event) {
        event.preventDefault();

        var IdEvento = $("#IdEvento").val();
        var IdStatus = $("#IdStatus").val();
        var txtName = $("#txtName").val();
        var txtComments = $("#txtComments").val();
        var txtDescription = $("#txtDescription").val();
        var txtReason = $("#txtReason").val();
        
        if (IdStatus === null || IdStatus === "" || typeof IdStatus === "undefined" || IdStatus == 0) {
            alerta("Atenção!", "Selecione o status.");
            return false;
        }

        if (txtName === null || txtName === "" || typeof txtName === "undefined") {
            alerta("Atenção!", "Preencha o titulo do evento.");
            return false;
        }

        if (txtDescription === null || txtDescription === "" || typeof txtDescription === "undefined") {
            alerta("Atenção!", "Preencha a descrição do evento.");
            return false;
        }
        
        if (IdStatus == 3) /** Evento Reprovado **/ {
            if (txtReason === null || txtReason === "" || typeof txtReason === "undefined") {
                alerta("Atenção!", "Informe o motivo da reprovação.");
                return false;
            }
        }

        var data = {
            IdEvent: IdEvento,
            IdStatus: IdStatus,
            Name: txtName,
            Comments: txtComments,
            Description: txtDescription,
            Reason: txtReason
        };

        // console.log(data);

        var formData = new FormData();
        formData.append("strDados", JSON.stringify(data));

        $("#loading").fadeIn();

        var url = "/Evento/Edit";
        //var url = "/beneficiodoposto/Evento/Edit";
        $.ajax({
            type: "POST",
            url: url,
            //contentType: "multipart/form-data",
            data: formData,
            contentType: false,
            processData: false,
            success: function (response) {
                if (response) {
                    $("#loading").fadeOut();
                    alerta("Sucesso!", "Evento atualizado com sucesso.", false, "/Evento/");
                    //alerta("Sucesso!", "Evento atualizado com sucesso.", false, "/beneficiodoposto/Evento/");
                }
                else {
                    $("#loading").fadeOut();
                    alerta("Atenção!", "Erro ao atualizar evento");
                }
            },
            error: function () {
                $("#loading").fadeOut();
                alerta("Atenção!", "Não foi possível atualizar evento.");
            },
            dataType: "json",
            //traditional: true
        });
        return false;
    });
});
﻿$(document).ready(function () {

    $("#dvReason").hide();
    $("#btnSalvar").hide();

    // STATUS
    $("#IdStatus").on("change", function () {
        var value = $(this).val();
        var dvReason = $("#dvReason");

        if (value == 3) /** Benefício Reprovado **/ {
            dvReason.show();
        } else {
            dvReason.hide();
        }

        $("#btnSalvar").show();
    });

    $("#txtDescription").on("input", function () {
        $("#btnSalvar").show();
    });

    $("#txtComments").on("input", function () {
        $("#btnSalvar").show();
    });

    // VOLTAR
    $("#btnVoltar").on("click", function (event) {
        event.preventDefault();
        window.location.href = "/Beneficio/";
        //window.location.href = "/beneficiodoposto/Beneficio/";
    });

    // SALVA
    $("#btnSalvar").click(function (event) {
        event.preventDefault();

        var IdBenefit = $("#IdBenefit").val();
        var IdStatus = $("#IdStatus").val();
        var txtComments = $("#txtComments").val();
        var txtDescription = $("#txtDescription").val();
        var txtReason = $("#txtReason").val();
        
        if (IdStatus === null || IdStatus === "" || typeof IdStatus === "undefined" || IdStatus == 0) {
            alerta("Atenção!", "Selecione o status.");
            return false;
        }

        if (txtDescription === null || txtDescription === "" || typeof txtDescription === "undefined") {
            alerta("Atenção!", "Preencha a descrição do funcionamento da mecânica para a entrega do benefício.");
            return false;
        }
        
        if (IdStatus == 3) /** Benefício Reprovado **/ {
            if (txtReason === null || txtReason === "" || typeof txtReason === "undefined") {
                alerta("Atenção!", "Informe o motivo da reprovação.");
                return false;
            }
        }

        var data = {
            IdBenefit: IdBenefit,
            IdStatus: IdStatus,
            Comments: txtComments,
            Description: txtDescription,
            Reason: txtReason
        };

        // console.log(data);

        var formData = new FormData();
        formData.append("strDados", JSON.stringify(data));

        $("#loading").fadeIn();

        var url = "/Beneficio/Edit";
        //var url = "/beneficiodoposto/Beneficio/Edit";
        $.ajax({
            type: "POST",
            url: url,
            //contentType: "multipart/form-data",
            data: formData,
            contentType: false,
            processData: false,
            success: function (response) {
                if (response) {
                    $("#loading").fadeOut();
                    alerta("Sucesso!", "Benefício atualizado com sucesso.", false, "/Beneficio/");
                    //alerta("Sucesso!", "Benefício atualizado com sucesso.", false, "/beneficiodoposto/Beneficio/");
                }
                else {
                    $("#loading").fadeOut();
                    alerta("Atenção!", "Erro ao atualizar benefício");
                }
            },
            error: function () {
                $("#loading").fadeOut();
                alerta("Atenção!", "Não foi possível atualizar benefício.");
            },
            dataType: "json",
            //traditional: true
        });
        return false;
    });
});
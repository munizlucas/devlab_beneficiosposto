﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Drawing.Spreadsheet;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;


/// <summary>
/// Summary description for ExcelStructure
/// </summary>
public class ExcelStructure
{
    #region constants

    public const int TYPE_STRING = 0;
    public const int TYPE_SHARED_STRING = 1;
    public const int TYPE_INLINE_STRING = 2;
    public const int TYPE_NUMBER = 3;
    public const int TYPE_DATE = 4;
    public const int TYPE_BOOLEAN = 5;
    public const int TYPE_ERROR = 6;
    public const int TYPE_FORMULA = 10;

    #endregion
    #region properties
    private SpreadsheetDocument _spreadsheetDocument;
    public SpreadsheetDocument SpreadsheetDocument
    {
        get { return _spreadsheetDocument; }
        set { _spreadsheetDocument = value; }
    }

    FileVersion _fileVersion;
    public FileVersion fileVersion
    {
        get { return _fileVersion; }
        set { _fileVersion = value; }
    }

    Workbook _workbook;
    public Workbook workbook
    {
        get { return _workbook; }
        set { _workbook = value; }
    }

    WorkbookPart _workbookpart;
    public WorkbookPart workbookpart
    {
        get { return _workbookpart; }
        set { _workbookpart = value; }
    }

    WorkbookStylesPart _stylesPart;
    public WorkbookStylesPart stylesPart
    {
        get { return _stylesPart; }
        set { _stylesPart = value; }
    }

    Stylesheet _stylesSheet;
    public Stylesheet stylesSheet
    {
        get { return _stylesSheet; }
        set { _stylesSheet = value; }
    }

    WorksheetPart _worksheetPart;
    public WorksheetPart worksheetPart
    {
        get { return _worksheetPart; }
        set { _worksheetPart = value; }
    }

    Worksheet _worksheet;
    public Worksheet worksheet
    {
        get { return _worksheet; }
        set { _worksheet = value; }
    }

    Sheets _sheets;
    public Sheets sheets
    {
        get { return _sheets; }
        set { _sheets = value; }
    }

    Sheet _sheet;
    public Sheet sheet
    {
        get { return _sheet; }
        set { _sheet = value; }
    }

    Columns _columns;
    public Columns columns
    {
        get { return _columns; }
        set { _columns = value; }
    }

    SheetData _sheetData;
    public SheetData sheetData
    {
        get { return _sheetData; }
        set { _sheetData = value; }
    }

    DrawingsPart _drawingsPart;
    public DrawingsPart drawingsPart
    {
        get { return _drawingsPart; }
        set { _drawingsPart = value; }
    }

    WorksheetDrawing _worksheetDrawing;
    public WorksheetDrawing worksheetDrawing
    {
        get { return _worksheetDrawing; }
        set { _worksheetDrawing = value; }
    }

    ImagePart _imagePart;
    public ImagePart imagePart
    {
        get { return _imagePart; }
        set { _imagePart = value; }
    }


    #endregion

    //Create the structure in memory
    public ExcelStructure(ref MemoryStream stream, bool autoSave = true)
    {
        // Create a spreadsheet document by supplying the filepath.
        // By default, AutoSave = true, Editable = true, and Type = xlsx.
        _spreadsheetDocument = SpreadsheetDocument.Create(stream, SpreadsheetDocumentType.Workbook, autoSave);

        generateFile();
    }

    //Create the structure in memory
    public ExcelStructure(string sheetName, ref MemoryStream stream, bool autoSave = true)
    {
        // Create a spreadsheet document by supplying the filepath.
        // By default, AutoSave = true, Editable = true, and Type = xlsx.
        _spreadsheetDocument = SpreadsheetDocument.Create(stream, SpreadsheetDocumentType.Workbook, autoSave);

        generateFile(sheetName);
    }

    //Create the structure from a file path
    public ExcelStructure(string path, bool autoSave = true)
    {
        // Create a spreadsheet document by supplying the filepath.
        // By default, AutoSave = true, Editable = true, and Type = xlsx.
        _spreadsheetDocument = SpreadsheetDocument.Create(path, SpreadsheetDocumentType.Workbook, autoSave);

        generateFile();
    }

    //Create the structure from a file path
    public ExcelStructure(string sheetName, string path, bool autoSave = true)
    {
        // Create a spreadsheet document by supplying the filepath.
        // By default, AutoSave = true, Editable = true, and Type = xlsx.
        _spreadsheetDocument = SpreadsheetDocument.Create(path, SpreadsheetDocumentType.Workbook, autoSave);

        generateFile(sheetName);
    }

    private void generateFile(string sheetName = "mySheet")
    {
        // Add a WorkbookPart to the document.
        _workbook = new Workbook();
        _workbookpart = SpreadsheetDocument.AddWorkbookPart();
        _workbookpart.Workbook = _workbook;

        _fileVersion = new FileVersion
        {
            ApplicationName = "Microsoft Office Excel"
        };

        _workbook.Append(_fileVersion);

        // Add a WorksheetPart to the WorkbookPart.
        _worksheet = new Worksheet();
        _worksheetPart = workbookpart.AddNewPart<WorksheetPart>();
        _worksheetPart.Worksheet = _worksheet;

        _stylesPart = _spreadsheetDocument.WorkbookPart.AddNewPart<WorkbookStylesPart>();
        _stylesSheet = generateStyleSheet();
        _stylesPart.Stylesheet = _stylesSheet;

        stylesPart.Stylesheet.Save();

        // Add Sheets to the Workbook.
        _sheets = SpreadsheetDocument.WorkbookPart.Workbook.AppendChild(new Sheets());

        // Append a new worksheet and associate it with the workbook.
        _sheet = new Sheet() { Id = SpreadsheetDocument.WorkbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = sheetName };
        _sheets.Append(sheet);

        _columns = new Columns();
        _worksheet.Append(_columns);
    }

    // create first sheetData cell table.
    public void createSheetData()
    {
        _sheetData = new SheetData();
        _worksheet.Append(_sheetData);
    }

    public void close()
    {
        _spreadsheetDocument.Close();
    }

    //Diagram diagram, ImageSetup setup
    public void insertImageOnLeftTop(string sImagePath, int heightInExcelUnit)
    {
        DrawingsPart dp = _worksheetPart.AddNewPart<DrawingsPart>();
        ImagePart imgp = dp.AddImagePart(ImagePartType.Jpeg, _worksheetPart.GetIdOfPart(dp));

        using (FileStream fs = new FileStream(sImagePath, FileMode.Open))
        {
            imgp.FeedData(fs);
        }
        NonVisualDrawingProperties nvdp = new NonVisualDrawingProperties
        {
            Id = 1025,
            Name = "Picture 1",
            Description = "Header Image"
        };
        DocumentFormat.OpenXml.Drawing.PictureLocks picLocks = new DocumentFormat.OpenXml.Drawing.PictureLocks
        {
            NoChangeAspect = true,
            NoChangeArrowheads = true
        };
        NonVisualPictureDrawingProperties nvpdp = new NonVisualPictureDrawingProperties
        {
            PictureLocks = picLocks
        };
        NonVisualPictureProperties nvpp = new NonVisualPictureProperties
        {
            NonVisualDrawingProperties = nvdp,
            NonVisualPictureDrawingProperties = nvpdp
        };

        DocumentFormat.OpenXml.Drawing.Stretch stretch = new DocumentFormat.OpenXml.Drawing.Stretch
        {
            FillRectangle = new DocumentFormat.OpenXml.Drawing.FillRectangle()
        };

        BlipFill blipFill = new BlipFill();
        DocumentFormat.OpenXml.Drawing.Blip blip = new DocumentFormat.OpenXml.Drawing.Blip
        {
            Embed = dp.GetIdOfPart(imgp),
            CompressionState = DocumentFormat.OpenXml.Drawing.BlipCompressionValues.Print
        };
        blipFill.Blip = blip;
        blipFill.SourceRectangle = new DocumentFormat.OpenXml.Drawing.SourceRectangle();
        blipFill.Append(stretch);

        DocumentFormat.OpenXml.Drawing.Transform2D t2d = new DocumentFormat.OpenXml.Drawing.Transform2D();
        DocumentFormat.OpenXml.Drawing.Offset offset = new DocumentFormat.OpenXml.Drawing.Offset
        {
            X = 0,
            Y = 0
        };
        t2d.Offset = offset;
        System.Drawing.Bitmap bm = new System.Drawing.Bitmap(sImagePath);
        DocumentFormat.OpenXml.Drawing.Extents extents = new DocumentFormat.OpenXml.Drawing.Extents
        {
            Cx = bm.Width * (long)(914400 / bm.HorizontalResolution),
            Cy = bm.Height * (long)(914400 / bm.VerticalResolution)
        };
        bm.Dispose();
        t2d.Extents = extents;
        ShapeProperties sp = new ShapeProperties
        {
            BlackWhiteMode = DocumentFormat.OpenXml.Drawing.BlackWhiteModeValues.Auto,
            Transform2D = t2d
        };
        DocumentFormat.OpenXml.Drawing.PresetGeometry prstGeom = new DocumentFormat.OpenXml.Drawing.PresetGeometry();
        prstGeom.Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle;
        prstGeom.AdjustValueList = new DocumentFormat.OpenXml.Drawing.AdjustValueList();
        sp.Append(prstGeom);
        sp.Append(new DocumentFormat.OpenXml.Drawing.NoFill());

        DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture picture = new DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture
        {
            NonVisualPictureProperties = nvpp,
            BlipFill = blipFill,
            ShapeProperties = sp
        };

        Position pos = new Position
        {
            X = 1828000,
            Y = 45700
        };
        Extent ext = new Extent
        {
            Cx = extents.Cx,
            Cy = extents.Cy
        };
        AbsoluteAnchor anchor = new AbsoluteAnchor
        {
            Position = pos,
            Extent = ext
        };
        anchor.Append(picture);
        anchor.Append(new ClientData());
        WorksheetDrawing wsd = new WorksheetDrawing();
        wsd.Append(anchor);
        Drawing drawing = new Drawing
        {
            Id = dp.GetIdOfPart(imgp)
        };

        wsd.Save(dp);

        _worksheet.Append(drawing);

        setRowHeight(1, heightInExcelUnit);

        _worksheet.Save();

        _workbook.Save();
    }

    //Diagram diagram, ImageSetup setup
    public void insertImageOnPosition(string sImagePath, int heightInExcelUnit, double posX, double posY)
    {
        if (_drawingsPart == null)
        {
            _drawingsPart = _worksheetPart.AddNewPart<DrawingsPart>();
            _imagePart = _drawingsPart.AddImagePart(ImagePartType.Jpeg, _worksheetPart.GetIdOfPart(_drawingsPart));
            _worksheetDrawing = new WorksheetDrawing();
        }
        else
        {
            _imagePart = _drawingsPart.AddImagePart(ImagePartType.Jpeg);
            _drawingsPart.CreateRelationshipToPart(_imagePart);
        }

        using (FileStream fs = new FileStream(sImagePath, FileMode.Open))
        {
            _imagePart.FeedData(fs);
        }

        NonVisualDrawingProperties nvdp = new NonVisualDrawingProperties
        {
            Id = 1025,
            Name = "Picture 1",
            Description = "Header Image"
        };
        DocumentFormat.OpenXml.Drawing.PictureLocks picLocks = new DocumentFormat.OpenXml.Drawing.PictureLocks
        {
            NoChangeAspect = true,
            NoChangeArrowheads = true
        };
        NonVisualPictureDrawingProperties nvpdp = new NonVisualPictureDrawingProperties
        {
            PictureLocks = picLocks
        };
        NonVisualPictureProperties nvpp = new NonVisualPictureProperties
        {
            NonVisualDrawingProperties = nvdp,
            NonVisualPictureDrawingProperties = nvpdp
        };

        DocumentFormat.OpenXml.Drawing.Stretch stretch = new DocumentFormat.OpenXml.Drawing.Stretch
        {
            FillRectangle = new DocumentFormat.OpenXml.Drawing.FillRectangle()
        };

        BlipFill blipFill = new BlipFill();
        DocumentFormat.OpenXml.Drawing.Blip blip = new DocumentFormat.OpenXml.Drawing.Blip
        {
            Embed = _drawingsPart.GetIdOfPart(_imagePart),
            CompressionState = DocumentFormat.OpenXml.Drawing.BlipCompressionValues.Print
        };
        blipFill.Blip = blip;
        blipFill.SourceRectangle = new DocumentFormat.OpenXml.Drawing.SourceRectangle();
        blipFill.Append(stretch);

        DocumentFormat.OpenXml.Drawing.Transform2D t2d = new DocumentFormat.OpenXml.Drawing.Transform2D();
        DocumentFormat.OpenXml.Drawing.Offset offset = new DocumentFormat.OpenXml.Drawing.Offset
        {
            X = 0,
            Y = 0
        };
        t2d.Offset = offset;
        System.Drawing.Bitmap bm = new System.Drawing.Bitmap(sImagePath);
        DocumentFormat.OpenXml.Drawing.Extents extents = new DocumentFormat.OpenXml.Drawing.Extents
        {
            Cx = bm.Width * (long)(914400 / bm.HorizontalResolution),
            Cy = bm.Height * (long)(914400 / bm.VerticalResolution)
        };
        bm.Dispose();
        t2d.Extents = extents;
        ShapeProperties sp = new ShapeProperties();
        sp.BlackWhiteMode = DocumentFormat.OpenXml.Drawing.BlackWhiteModeValues.Auto;
        sp.Transform2D = t2d;
        DocumentFormat.OpenXml.Drawing.PresetGeometry prstGeom = new DocumentFormat.OpenXml.Drawing.PresetGeometry();
        prstGeom.Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle;
        prstGeom.AdjustValueList = new DocumentFormat.OpenXml.Drawing.AdjustValueList();
        sp.Append(prstGeom);
        sp.Append(new DocumentFormat.OpenXml.Drawing.NoFill());

        DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture picture = new DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture
        {
            NonVisualPictureProperties = nvpp,
            BlipFill = blipFill,
            ShapeProperties = sp
        };

        Position pos = new Position
        {
            X = (long)posX,
            Y = (long)posY
        };
        Extent ext = new Extent
        {
            Cx = extents.Cx,
            Cy = extents.Cy
        };
        AbsoluteAnchor anchor = new AbsoluteAnchor
        {
            Position = pos,
            Extent = ext
        };
        anchor.Append(picture);
        anchor.Append(new ClientData());

        _worksheetDrawing.Append(anchor);
        Drawing drawing = new Drawing
        {
            Id = _drawingsPart.GetIdOfPart(_imagePart)
        };

        _worksheetDrawing.Save(_drawingsPart);

        _worksheet.Append(drawing);

        setRowHeight(1, heightInExcelUnit);

        _worksheet.Save();

        _workbook.Save();
    }

    public Row CreateHeader(UInt32 index, string headerText)
    {
        Row r = new Row
        {
            RowIndex = index
        };

        Cell c = new Cell
        {
            DataType = CellValues.String,
            StyleIndex = 5,
            CellReference = "A" + index.ToString(),
            CellValue = new CellValue(headerText)
        };
        r.Append(c);

        _sheetData.Append(r);

        return r;
    }

    public Row CreateHeader(UInt32 index, string headerText, int fontSize, int mergeColumns = 0)
    {
        Row r = new Row
        {
            RowIndex = index
        };

        Cell c = new Cell
        {
            DataType = CellValues.String,
            StyleIndex = 5
        };

        CellFormat x = (CellFormat)_stylesSheet.CellFormats.ElementAt(5);
        x.Alignment = new Alignment
        {
            Horizontal = HorizontalAlignmentValues.Left
        };
        Font y = (Font)_stylesSheet.Fonts.ElementAt(int.Parse(x.FontId));
        y.FontSize = new FontSize() { Val = fontSize };

        c.CellReference = "A" + index.ToString();
        c.CellValue = new CellValue(headerText);

        if (mergeColumns > 0)
            mergeCells("A" + index, formatValue(mergeColumns) + index);

        r.Append(c);

        _sheetData.Append(r);

        return r;
    }

    public void CreateColumnHeader(UInt32 index, IList<string> camposHeader)
    {
        Row r = new Row
        {
            RowIndex = index
        };

        int col = 0;

        foreach (var campo in camposHeader)
        {
            Cell c;
            c = new Cell();
            c.DataType = CellValues.String;
            c.CellReference = formatValue(col) + index.ToString();
            c.CellValue = new CellValue(campo);
            c.StyleIndex = 1;

            r.Append(c);
            col++;
        }

        _sheetData.Append(r);
    }

    public void CreateColumnHeader(UInt32 index, IList<string> camposHeader, bool mergeFirstRow)
    {
        Row r = new Row
        {
            RowIndex = index
        };

        int col = 0;

        foreach (var campo in camposHeader)
        {
            Cell c;
            c = new Cell();
            c.DataType = CellValues.String;
            c.CellReference = formatValue(col) + index.ToString();
            c.CellValue = new CellValue(campo);
            c.StyleIndex = 1;
            r.Append(c);

            col++;
        }

        if (mergeFirstRow)
            mergeCells("A1", formatValue(col - 1) + "1");

        _sheetData.Append(r);
    }

    // convert 0 to A, 1 to B, ... 17576 to ZZZ
    private string formatValue(int i)
    {
        var result = new StringBuilder();

        int ind = 1;

        if (i < 26)
            ind = 0;
        else if (i < 52)
            result.Insert(0, (char)('A'));
        else if (i < 78)
            result.Insert(0, (char)('B'));
        else if (i < 104)
            result.Insert(0, (char)('C'));
        else if (i < 130)
            result.Insert(0, (char)('D'));
        else if (i < 156)
            result.Insert(0, (char)('E'));
        else if (i < 182)
            result.Insert(0, (char)('F'));
        else if (i < 208)
            result.Insert(0, (char)('G'));
        else if (i < 234)
            result.Insert(0, (char)('H'));
        else if (i < 260)
            result.Insert(0, (char)('I'));
        else if (i < 286)
            result.Insert(0, (char)('J'));
        else if (i < 312)
            result.Insert(0, (char)('K'));
        else if (i < 338)
            result.Insert(0, (char)('L'));
        else if (i < 364)
            result.Insert(0, (char)('M'));
        else if (i < 390)
            result.Insert(0, (char)('N'));
        else if (i < 416)
            result.Insert(0, (char)('O'));
        else if (i < 442)
            result.Insert(0, (char)('P'));
        else if (i < 468)
            result.Insert(0, (char)('Q'));
        else if (i < 494)
            result.Insert(0, (char)('R'));
        else if (i < 520)
            result.Insert(0, (char)('S'));
        else if (i < 546)
            result.Insert(0, (char)('T'));
        else if (i < 572)
            result.Insert(0, (char)('U'));
        else if (i < 598)
            result.Insert(0, (char)('V'));
        else if (i < 624)
            result.Insert(0, (char)('W'));
        else if (i < 650)
            result.Insert(0, (char)('X'));
        else if (i < 676)
            result.Insert(0, (char)('Y'));
        else if (i < 702)
            result.Insert(0, (char)('Z'));

        result.Insert(ind, (char)('A' + (i % 26)));

        return result.ToString();
    }

    public void CreateRowWithStringContent(UInt32 index, string[] valores)
    {
        Row r = new Row
        {
            RowIndex = index
        };

        for (int i = 0; i < valores.Length; i++)
        {
            var c = new Cell
            {
                CellReference = formatValue(i) + index.ToString(),
                CellValue = new CellValue(valores[i]),
                StyleIndex = 6,
                DataType = CellValues.String
            };
            r.Append(c);
        }

        _sheetData.Append(r);
    }

    public void CreateRowWithContent(UInt32 index, IList<string> valores, IList<int> tipo)
    {
        if (valores.Count == tipo.Count)
        {
            var r = new Row
            {
                RowIndex = index
            };

            for (int i = 0; i < valores.Count; i++)
            {
                Cell c = new Cell
                {
                    CellReference = formatValue(i) + index.ToString(),
                    CellValue = new CellValue(valores[i]),
                    StyleIndex = 6
                };
                switch (tipo[i])
                {
                    case TYPE_STRING:
                        c.DataType = CellValues.String;
                        break;
                    case TYPE_SHARED_STRING:
                        c.DataType = CellValues.SharedString;
                        break;
                    case TYPE_INLINE_STRING:
                        c.DataType = CellValues.InlineString;
                        break;
                    case TYPE_NUMBER:
                        c.DataType = CellValues.Number;
                        break;
                    case TYPE_DATE:
                        c.DataType = CellValues.Date;
                        break;
                    case TYPE_BOOLEAN:
                        c.DataType = CellValues.Boolean;
                        break;
                    case TYPE_ERROR:
                        c.DataType = CellValues.Error;
                        break;
                }

                r.Append(c);
            }
            _sheetData.Append(r);
        }
    }

    public void CreateSummary(UInt32 index, IList<string> valores, IList<int> tipo)
    {
        if (valores.Count == tipo.Count)
        {
            var r = new Row
            {
                RowIndex = index
            };

            for (int i = 0; i < valores.Count; i++)
            {
                var c = new Cell
                {
                    CellReference = formatValue(i) + index.ToString()
                };
                if (tipo[i] == TYPE_FORMULA)
                {
                    c.DataType = new EnumValue<CellValues>(CellValues.Number);
                    c.CellFormula = new CellFormula(valores[i]);
                    c.StyleIndex = 6;
                }
                else
                {
                    c.CellValue = new CellValue(valores[i]);
                    c.StyleIndex = 6;
                    switch (tipo[i])
                    {
                        case TYPE_STRING:
                            c.DataType = CellValues.String;
                            break;
                        case TYPE_SHARED_STRING:
                            c.DataType = CellValues.SharedString;
                            break;
                        case TYPE_INLINE_STRING:
                            c.DataType = CellValues.InlineString;
                            break;
                        case TYPE_NUMBER:
                            c.DataType = CellValues.Number;
                            break;
                        case TYPE_DATE:
                            c.DataType = CellValues.Date;
                            break;
                        case TYPE_BOOLEAN:
                            c.DataType = CellValues.Boolean;
                            break;
                        case TYPE_ERROR:
                            c.DataType = CellValues.Error;
                            break;
                    }
                }

                r.Append(c);
            }

            _sheetData.Append(r);
        }
    }


    public void save()
    {
        _workbook.Save();
    }

    public void setRowHeight(int rowIndex, int height)
    {
        Row row;

        try
        {
            row = worksheet.GetFirstChild<SheetData>().Elements<Row>().Where(r => r.RowIndex == rowIndex).First();

            row.Height = height;

            row.CustomHeight = true;
        }
        catch (Exception)
        {
            row = new Row
            {
                RowIndex = (uint)rowIndex,
                Height = height,
                CustomHeight = true
            };

            var cell = new Cell
            {
                DataType = CellValues.String,
                CellReference = formatValue(0) + rowIndex.ToString(),
                CellValue = new CellValue("")
            };

            row.Append(cell);

            sheetData.Append(row);
        }

        save();
    }

    public void mergeCells(string firstRangeCell, string lastRangeCell)
    {
        MergeCells mergeCells;

        if (worksheet.Elements<MergeCells>().Count() > 0)
        {
            mergeCells = worksheet.Elements<MergeCells>().First();
        }
        else
        {
            mergeCells = new MergeCells();

            // Insert a MergeCells object into the specified position.
            if (_worksheet.Elements<CustomSheetView>().Count() > 0)
            {
                _worksheet.InsertAfter(mergeCells, _worksheet.Elements<CustomSheetView>().First());
            }
            else if (_worksheet.Elements<DataConsolidate>().Count() > 0)
            {
                _worksheet.InsertAfter(mergeCells, _worksheet.Elements<DataConsolidate>().First());
            }
            else if (_worksheet.Elements<SortState>().Count() > 0)
            {
                _worksheet.InsertAfter(mergeCells, _worksheet.Elements<SortState>().First());
            }
            else if (_worksheet.Elements<AutoFilter>().Count() > 0)
            {
                _worksheet.InsertAfter(mergeCells, _worksheet.Elements<AutoFilter>().First());
            }
            else if (_worksheet.Elements<Scenarios>().Count() > 0)
            {
                _worksheet.InsertAfter(mergeCells, _worksheet.Elements<Scenarios>().First());
            }
            else if (_worksheet.Elements<ProtectedRanges>().Count() > 0)
            {
                _worksheet.InsertAfter(mergeCells, _worksheet.Elements<ProtectedRanges>().First());
            }
            else if (_worksheet.Elements<SheetProtection>().Count() > 0)
            {
                _worksheet.InsertAfter(mergeCells, _worksheet.Elements<SheetProtection>().First());
            }
            else if (_worksheet.Elements<SheetCalculationProperties>().Count() > 0)
            {
                _worksheet.InsertAfter(mergeCells, _worksheet.Elements<SheetCalculationProperties>().First());
            }
            else
            {
                _worksheet.InsertAfter(mergeCells, _worksheet.Elements<SheetData>().First());
            }
        }

        MergeCell mergeCell = new MergeCell() { Reference = new StringValue(firstRangeCell + ":" + lastRangeCell) };
        mergeCells.Append(mergeCell);

        _worksheet.Save();
    }

    public void setColumnWidth(uint firstRangeCol, uint lastRangeCol, int width)
    {
        var xlCol = new Column
        {
            Width = width,
            BestFit = true,
            CustomWidth = true,
            Min = firstRangeCol,
            Max = lastRangeCol
        };
        _columns.Append(xlCol);

        _worksheet.Save();
    }

    public void setColumnWidth(uint column, int width)
    {
        var xlCol = new Column
        {
            Width = width,
            BestFit = true,
            CustomWidth = true,
            Min = column,
            Max = column
        };
        _columns.Append(xlCol);

        _worksheet.Save();
    }

    private Stylesheet generateStyleSheet()
    {
        return new Stylesheet(
            new Fonts(
                new Font(                                                               // Index 0 - The default font.
                    new FontSize() { Val = 11 },
                    new Color() { Rgb = new HexBinaryValue("00000000") },
                    new FontName() { Val = "Calibri" }),
                new Font(                                                               // Index 1 - The bold font.
                    new Bold(),
                    new FontSize() { Val = 11 },
                    new Color() { Rgb = new HexBinaryValue("00000000") },
                    new FontName() { Val = "Calibri" }),
                new Font(                                                               // Index 2 - The Italic font.
                    new Italic(),
                    new FontSize() { Val = 11 },
                    new Color() { Rgb = new HexBinaryValue("00000000") },
                    new FontName() { Val = "Calibri" }),
                new Font(                                                               // Index 3 - The Times Roman font. with 16 size
                    new Bold(),
                    new FontSize() { Val = 28 },
                    new Color() { Rgb = new HexBinaryValue("00000000") },
                    new FontName() { Val = "Calibri" })
            ),
            new Fills(
                new Fill(                                                           // Index 0 - The default fill.
                    new PatternFill() { PatternType = PatternValues.None }),
                new Fill(                                                           // Index 1 - The default fill of gray 125 (required)
                    new PatternFill() { PatternType = PatternValues.Gray125 }),
                new Fill(                                                           // Index 2 - The yellow fill.
                    new PatternFill(
                        new ForegroundColor() { Rgb = new HexBinaryValue() { Value = "FFFFFF00" } }
                    )
                    { PatternType = PatternValues.Solid })
            ),
            new Borders(
                new Border(                                                         // Index 0 - The default border.
                    new LeftBorder(),
                    new RightBorder(),
                    new TopBorder(),
                    new BottomBorder(),
                    new DiagonalBorder()),
                new Border(                                                         // Index 1 - Applies a Left, Right, Top, Bottom border to a cell
                    new LeftBorder(
                    /* new Color() { Auto = true } */
                    )
                    { Style = BorderStyleValues.Thin, Color = new Color() { Rgb = new HexBinaryValue("00000000") } },
                    new RightBorder(
                    /* new Color() { Auto = true } */
                    )
                    { Style = BorderStyleValues.Thin, Color = new Color() { Rgb = new HexBinaryValue("00000000") } },
                    new TopBorder(
                    /* new Color() { Auto = true } */
                    )
                    { Style = BorderStyleValues.Thin, Color = new Color() { Rgb = new HexBinaryValue("00000000") } },
                    new BottomBorder(
                    /* new Color() { Auto = true } */
                    )
                    { Style = BorderStyleValues.Thin, Color = new Color() { Rgb = new HexBinaryValue("00000000") } },
                    new DiagonalBorder())
            ),
            new CellFormats(
                new CellFormat() { FontId = 0, FillId = 0, BorderId = 0 },                          // Index 0 - The default cell style.  If a cell does not have a style index applied it will use this style combination instead
                new CellFormat() { FontId = 1, FillId = 0, BorderId = 1, ApplyFont = true },       // Index 1 - Bold 
                new CellFormat() { FontId = 2, FillId = 0, BorderId = 0, ApplyFont = true },       // Index 2 - Italic
                new CellFormat() { FontId = 3, FillId = 0, BorderId = 0, ApplyFont = true },       // Index 3 - Times Roman
                new CellFormat() { FontId = 0, FillId = 2, BorderId = 0, ApplyFill = true },       // Index 4 - Yellow Fill
                new CellFormat(                                                                   // Index 5 - Alignment
                    new Alignment() { Horizontal = HorizontalAlignmentValues.Center, Vertical = VerticalAlignmentValues.Center }
                )
                { FontId = 3, FillId = 0, BorderId = 0, ApplyAlignment = true },
                new CellFormat() { FontId = 0, FillId = 0, BorderId = 1, ApplyBorder = true }      // Index 6 - Border
            )
        ); // return
    }

    /*
     * 1emu = ( 1 / 360000 )cm
     * 
     * 1cm = 360000emu
     * */
    public static double convertCMToEMU(double value)
    {
        double result = 0;

        result = value * 360000;

        return result;
    }

    /*
     * 1emu = ( 1 / 914400 )in
     * 
     * 1in = 914400emu
     * */
    public static double convertInchsToEMU(double value)
    {
        double result = 0;

        result = value * 914400;

        return result;
    }

    /*
     * 1in = 13excelunits
     * 
     * 1excelunit = 1/13in
     * 
     * 1emu = (1/11887200)excelunits
     * 
     * 1excelunit = 11887200emu
     * */
    public static double convertExcelPointsToEMU(double value)
    {
        double result = 0;

        result = value / 13.71;

        result = convertInchsToEMU(result);

        /*  result = (result) * 914400; */

        //result = convertInchsToEMU(result);

        return result;
    }

    public static void InsertImage(Worksheet ws, long x, long y, long? width, long? height, string sImagePath)
    {
        try
        {
            WorksheetPart wsp = ws.WorksheetPart;
            DrawingsPart dp;
            ImagePart imgp;
            WorksheetDrawing wsd;

            ImagePartType ipt;
            switch (sImagePath.Substring(sImagePath.LastIndexOf('.') + 1).ToLower())
            {
                case "png":
                    ipt = ImagePartType.Png;
                    break;
                case "jpg":
                case "jpeg":
                    ipt = ImagePartType.Jpeg;
                    break;
                case "gif":
                    ipt = ImagePartType.Gif;
                    break;
                default:
                    return;
            }

            if (wsp.DrawingsPart == null)
            {
                //----- no drawing part exists, add a new one

                dp = wsp.AddNewPart<DrawingsPart>();
                imgp = dp.AddImagePart(ipt, wsp.GetIdOfPart(dp));
                wsd = new WorksheetDrawing();
            }
            else
            {
                //----- use existing drawing part

                dp = wsp.DrawingsPart;
                imgp = dp.AddImagePart(ipt);
                dp.CreateRelationshipToPart(imgp);
                wsd = dp.WorksheetDrawing;
            }

            using (FileStream fs = new FileStream(sImagePath, FileMode.Open))
            {
                imgp.FeedData(fs);
            }

            int imageNumber = dp.ImageParts.Count<ImagePart>();
            if (imageNumber == 1)
            {
                Drawing drawing = new Drawing
                {
                    Id = dp.GetIdOfPart(imgp)
                };
                ws.Append(drawing);
            }

            NonVisualDrawingProperties nvdp = new NonVisualDrawingProperties
            {
                Id = new UInt32Value((uint)(1024 + imageNumber)),
                Name = "Picture " + imageNumber.ToString(),
                Description = ""
            };
            DocumentFormat.OpenXml.Drawing.PictureLocks picLocks = new DocumentFormat.OpenXml.Drawing.PictureLocks
            {
                NoChangeAspect = true,
                NoChangeArrowheads = true
            };
            NonVisualPictureDrawingProperties nvpdp = new NonVisualPictureDrawingProperties
            {
                PictureLocks = picLocks
            };
            NonVisualPictureProperties nvpp = new NonVisualPictureProperties
            {
                NonVisualDrawingProperties = nvdp,
                NonVisualPictureDrawingProperties = nvpdp
            };

            DocumentFormat.OpenXml.Drawing.Stretch stretch = new DocumentFormat.OpenXml.Drawing.Stretch
            {
                FillRectangle = new DocumentFormat.OpenXml.Drawing.FillRectangle()
            };

            BlipFill blipFill = new BlipFill();
            DocumentFormat.OpenXml.Drawing.Blip blip = new DocumentFormat.OpenXml.Drawing.Blip
            {
                Embed = dp.GetIdOfPart(imgp),
                CompressionState = DocumentFormat.OpenXml.Drawing.BlipCompressionValues.Print
            };
            blipFill.Blip = blip;
            blipFill.SourceRectangle = new DocumentFormat.OpenXml.Drawing.SourceRectangle();
            blipFill.Append(stretch);

            DocumentFormat.OpenXml.Drawing.Transform2D t2d = new DocumentFormat.OpenXml.Drawing.Transform2D();
            DocumentFormat.OpenXml.Drawing.Offset offset = new DocumentFormat.OpenXml.Drawing.Offset
            {
                X = 0,
                Y = 0
            };
            t2d.Offset = offset;
            System.Drawing.Bitmap bm = new System.Drawing.Bitmap(sImagePath);

            DocumentFormat.OpenXml.Drawing.Extents extents = new DocumentFormat.OpenXml.Drawing.Extents();

            if (width == null)
                extents.Cx = bm.Width * (long)(914400 / bm.HorizontalResolution);
            else
                extents.Cx = width;

            if (height == null)
                extents.Cy = bm.Height * (long)(914400 / bm.VerticalResolution);
            else
                extents.Cy = height;

            bm.Dispose();
            t2d.Extents = extents;
            ShapeProperties sp = new ShapeProperties
            {
                BlackWhiteMode = DocumentFormat.OpenXml.Drawing.BlackWhiteModeValues.Auto,
                Transform2D = t2d
            };
            DocumentFormat.OpenXml.Drawing.PresetGeometry prstGeom = new DocumentFormat.OpenXml.Drawing.PresetGeometry
            {
                Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle,
                AdjustValueList = new DocumentFormat.OpenXml.Drawing.AdjustValueList()
            };
            sp.Append(prstGeom);
            sp.Append(new DocumentFormat.OpenXml.Drawing.NoFill());

            DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture picture = new DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture
            {
                NonVisualPictureProperties = nvpp,
                BlipFill = blipFill,
                ShapeProperties = sp
            };

            var pos = new Position
            {
                X = x,
                Y = y
            };
            var ext = new Extent
            {
                Cx = extents.Cx,
                Cy = extents.Cy
            };
            var anchor = new AbsoluteAnchor
            {
                Position = pos,
                Extent = ext
            };
            anchor.Append(picture);
            anchor.Append(new ClientData());
            wsd.Append(anchor);
            wsd.Save(dp);
        }
        catch (Exception ex)
        {
            throw ex; // or do something more interesting if you want
        }
    }

    public static void InsertImage(Worksheet ws, long x, long y, string sImagePath)
    {
        InsertImage(ws, x, y, null, null, sImagePath);
    }
}
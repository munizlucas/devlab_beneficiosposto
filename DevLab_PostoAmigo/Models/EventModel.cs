﻿using System;
using System.Collections.Generic;
using DevLab_PostoAmigo.Data;
using DevLab_PostoAmigo.Business.Return;

namespace DevLab_PostoAmigo.Models
{
    public class EventModel
    {
        public int IdEvent { get; set; }
        public long? Ibm { get; set; }
        public int? IdStatus { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public System.DateTime DateEvent { get; set; }
        public string HourStart { get; set; }
        public string HourEnd { get; set; }
        public string Comments { get; set; }
        public string Reason { get; set; }
        public System.DateTime DateCreate { get; set; }
        public Nullable<System.DateTime> DateUpdate { get; set; }

        public string NameStatus { get; set; }

        public List<Event> ListEvent { get; set; }
        public List<StatusEvent> ListStatusEvent { get; set; }
    }
}
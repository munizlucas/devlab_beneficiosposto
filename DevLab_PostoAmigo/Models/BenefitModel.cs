﻿using System;
using System.Collections.Generic;
using DevLab_PostoAmigo.Data;
using DevLab_PostoAmigo.Business.Return;

namespace DevLab_PostoAmigo.Models
{
    public class BenefitModel
    {
        public int IdBenefit { get; set; }
        public long? Ibm { get; set; }
        public int? IdStatus { get; set; }
        public int? IdType { get; set; }
        public string Description { get; set; }
        public string Comments { get; set; }
        public int Quantity { get; set; }
        public Nullable<System.DateTime> DateStart { get; set; }
        public Nullable<System.DateTime> DateEnd { get; set; }
        public string Reason { get; set; }
        public System.DateTime DateCreate { get; set; }
        public Nullable<System.DateTime> DateUpdate { get; set; }

        public string NameStatus { get; set; }
        public string NameType { get; set; }

        public List<Benefit> ListBenefit { get; set; }
        public List<TypeBenefit> ListTypeBenefit { get; set; }
        public List<StatusBenefit> ListStatusBenefit { get; set; }
    }
}
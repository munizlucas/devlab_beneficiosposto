﻿namespace DevLab_PostoAmigo.Models
{
    public class LoginModel
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Success { get; set; }
    }
}
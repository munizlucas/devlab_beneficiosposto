﻿using DevLab_PostoAmigo.Business.Util;
using DevLab_PostoAmigo.Business.Return;
using DevLab_PostoAmigo.Business;
using DevLab_PostoAmigo.Data;
using DevLab_PostoAmigo.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.IO;

namespace DevLab_PostoAmigo.Controllers
{
    public class BeneficioController : Controller
    {
        [SessionCheck]
        public ActionResult Index()
        {
            var user = Geral.RecuperaCookieAdmin();

            ViewBag.Name = user.Name.ToString();
            ViewBag.Email = user.Email.ToString();
            ViewBag.IdType = new SelectList(Business.TypeBenefit.ListType().Select(x => new SelectListItem { Value = x.IdType.ToString(), Text = x.Name }).OrderBy(o => o.Text).ToList(), "Value", "Text");
            ViewBag.IdStatus = new SelectList(Business.StatusBenefit.ListStatus().Select(x => new SelectListItem { Value = x.IdStatus.ToString(), Text = x.Name }).OrderBy(o => o.Text).ToList(), "Value", "Text");

            var model = new Models.BenefitModel();
            model.ListBenefit = Business.Benfit.ListBenefit(null, null, null); 

            return View(model);
        }

        [SessionCheck]
        [HttpGet]
        public ActionResult Index(int? id)
        {
            var user = Geral.RecuperaCookieAdmin();

            ViewBag.Name = user.Name.ToString();
            ViewBag.Email = user.Email.ToString();
            ViewBag.IdType = new SelectList(Business.TypeBenefit.ListType().Select(x => new SelectListItem { Value = x.IdType.ToString(), Text = x.Name }).OrderBy(o => o.Text).ToList(), "Value", "Text");
            ViewBag.IdStatus = new SelectList(Business.StatusBenefit.ListStatus().Select(x => new SelectListItem { Value = x.IdStatus.ToString(), Text = x.Name }).OrderBy(o => o.Text).ToList(), "Value", "Text", id.ToString());

            var model = new Models.BenefitModel();
            model.ListBenefit = Business.Benfit.ListBenefit(null, id, null);

            return View(model);
        }

        [SessionCheck]
        [HttpPost]
        public ActionResult Filtrar(Models.BenefitModel model)
        {
            var user = Geral.RecuperaCookieAdmin();

            ViewBag.Name = user.Name.ToString();
            ViewBag.Email = user.Email.ToString();
            ViewBag.IdType = new SelectList(Business.TypeBenefit.ListType().Select(x => new SelectListItem { Value = x.IdType.ToString(), Text = x.Name }).OrderBy(o => o.Text).ToList(), "Value", "Text");
            ViewBag.IdStatus = new SelectList(Business.StatusBenefit.ListStatus().Select(x => new SelectListItem { Value = x.IdStatus.ToString(), Text = x.Name }).OrderBy(o => o.Text).ToList(), "Value", "Text");

            model.ListBenefit = Business.Benfit.ListBenefit(model.Ibm, model.IdStatus, model.IdType);

            return View("Index", model);
        }

        [SessionCheck]
        [HttpGet]
        public ActionResult Export(long? Ibm, int? IdStatus, int? IdType)
        {

            ViewBag.IdType = new SelectList(Business.TypeBenefit.ListType().Select(x => new SelectListItem { Value = x.IdType.ToString(), Text = x.Name }).OrderBy(o => o.Text).ToList(), "Value", "Text");
            ViewBag.IdStatus = new SelectList(Business.StatusBenefit.ListStatus().Select(x => new SelectListItem { Value = x.IdStatus.ToString(), Text = x.Name }).OrderBy(o => o.Text).ToList(), "Value", "Text");

            var beneficios = Business.Benfit.ListBenefit(Ibm, IdStatus, IdType);
            // Create file
            DateTime now = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("Central Brazilian Standard Time"));
            string filename = "Relatorio_Beneficios_" + now.ToString("ddMMyyyyHHmm") + ".xlsx";

            // Criar excel
            MemoryStream stream = new MemoryStream();
            ExcelStructure excel = new ExcelStructure("Beneficios", ref stream);

            excel.setColumnWidth(1, 20);  // Ibm
            excel.setColumnWidth(2, 20);  // NameStatus
            excel.setColumnWidth(3, 20);  // NameType
            excel.setColumnWidth(4, 30);  // Description
            excel.setColumnWidth(5, 20);  // Comments
            excel.setColumnWidth(6, 20);  // Quantity
            excel.setColumnWidth(7, 20);  // DateStart
            excel.setColumnWidth(8, 20);  // DateEnd
            excel.setColumnWidth(9, 20);  // Reason
            excel.setColumnWidth(10, 20); // DateCreate
            excel.setColumnWidth(11, 20); // DateUpdate

            excel.createSheetData();

            IList<string> headers = new List<string>();
            IList<string> valores = new List<string>();
            IList<int> tipos = new List<int>();

            headers = new List<string>();

            headers.Add("Ibm");
            headers.Add("Status");
            headers.Add("Tipo");
            headers.Add("Descrição");
            headers.Add("Observações");
            headers.Add("Quantidade");
            headers.Add("Data Inicio");
            headers.Add("Data Termino");
            headers.Add("Motivo");
            headers.Add("Data Criação");
            headers.Add("Data Atualização");

            excel.CreateColumnHeader(1, headers);

            uint row = 2;
            foreach (var c in beneficios)
            {
                valores = new List<string>();
                tipos = new List<int>();

                valores.Add(c.Ibm + " ");
                tipos.Add(ExcelStructure.TYPE_STRING);

                valores.Add(c.NameStatus + " ");
                tipos.Add(ExcelStructure.TYPE_STRING);

                valores.Add(c.NameType + " ");
                tipos.Add(ExcelStructure.TYPE_STRING);

                valores.Add(c.Description + " ");
                tipos.Add(ExcelStructure.TYPE_STRING);

                valores.Add(c.Comments + " ");
                tipos.Add(ExcelStructure.TYPE_STRING);

                valores.Add(c.Quantity + " ");
                tipos.Add(ExcelStructure.TYPE_STRING);

                valores.Add(c.DateStart + " ");
                tipos.Add(ExcelStructure.TYPE_STRING);

                valores.Add(c.DateEnd + " ");
                tipos.Add(ExcelStructure.TYPE_STRING);

                valores.Add(c.Reason + " ");
                tipos.Add(ExcelStructure.TYPE_STRING);

                valores.Add(c.DateCreate + " ");
                tipos.Add(ExcelStructure.TYPE_STRING);

                valores.Add(c.DateUpdate + " ");
                tipos.Add(ExcelStructure.TYPE_STRING);

                excel.CreateRowWithContent(row, valores, tipos);
                row++;
            }

            excel.save();
            excel.close();

            stream.Position = 0;

            return new FileStreamResult(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            {
                FileDownloadName = filename
            };
        }

        [SessionCheck]
        public ActionResult Details(int id)
        {
            var user = Geral.RecuperaCookieAdmin();

            ViewBag.Name = user.Name.ToString();
            ViewBag.Email = user.Email.ToString();

            var statusBenefit = Business.StatusBenefit.ListStatus();
            var typeBenefit = Business.TypeBenefit.ListType();

            Benefit benefit = Business.Benfit.Details(id);
            Models.BenefitModel model = new Models.BenefitModel
            {
                IdBenefit = benefit.IdBenefit,
                Ibm = benefit.Ibm,
                IdStatus = benefit.IdStatus,
                IdType = benefit.IdType,
                Description = benefit.Description,
                Comments = benefit.Comments,
                Quantity = benefit.Quantity,
                DateStart = benefit.DateStart,
                DateEnd = benefit.DateEnd,
                Reason = benefit.Reason,
                DateCreate = benefit.DateCreate,
                DateUpdate = benefit.DateUpdate,
                NameStatus = benefit.NameStatus,
                NameType = benefit.NameType,
                ListStatusBenefit = statusBenefit,
                ListTypeBenefit = typeBenefit,
            };

            return View(model);
        }

        [SessionCheck]
        [HttpPost]
        public ActionResult Edit(string strDados)
        {
            Models.BenefitModel dados;

            try
            {
                dados = JsonConvert.DeserializeObject<Models.BenefitModel>(strDados);

                Business.Benfit.Edit(dados.IdBenefit, (int)dados.IdStatus, dados.Description, dados.Comments, dados.Reason);
            }
            catch
            {
                
            }

            return Json(true);
        }
    }
}

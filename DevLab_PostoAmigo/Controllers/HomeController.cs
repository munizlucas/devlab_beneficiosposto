﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevLab_PostoAmigo.Filters;
using DevLab_PostoAmigo.Business;
using DevLab_PostoAmigo.Business.Util;

namespace DevLab_PostoAmigo.Controllers
{
    public class HomeController : BaseController
    {
        [SessionCheck]
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            var user = Geral.RecuperaCookieAdmin();

            ViewBag.Name = user.Name.ToString();
            ViewBag.Email = user.Email.ToString();

            var home = Home.Get();

            ViewBag.BenefitApproved = home.BenefitApproved;
            ViewBag.BenefitDisapproved = home.BenefitDisapproved;
            ViewBag.BenefitCancel = home.BenefitCancel;
            ViewBag.BenefitPending = home.BenefitPending;

            ViewBag.EventApproved = home.EventApproved;
            ViewBag.EventDisapproved = home.EventDisapproved;
            ViewBag.EventCancel = home.EventCancel;
            ViewBag.EventPending = home.EventPending;

            return View();
        }
    }
}

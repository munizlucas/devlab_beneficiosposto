﻿using DevLab_PostoAmigo.Business.Util;
using DevLab_PostoAmigo.Business.Return;
using DevLab_PostoAmigo.Data;
using DevLab_PostoAmigo.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.IO;

namespace DevLab_PostoAmigo.Controllers
{
    public class EventoController : Controller
    {
        [SessionCheck]
        public ActionResult Index()
        {
            var user = Geral.RecuperaCookieAdmin();

            ViewBag.Name = user.Name.ToString();
            ViewBag.Email = user.Email.ToString();
            ViewBag.IdStatus = new SelectList(Business.StatusEvent.ListStatus().Select(x => new SelectListItem { Value = x.IdStatus.ToString(), Text = x.Name }).OrderBy(o => o.Text).ToList(), "Value", "Text");

            var model = new Models.EventModel();
            model.ListEvent = Business.Event.ListEvent(null, null);

            return View(model);
        }

        [SessionCheck]
        [HttpGet]
        public ActionResult Index(int? id)
        {
            var user = Geral.RecuperaCookieAdmin();

            ViewBag.Name = user.Name.ToString();
            ViewBag.Email = user.Email.ToString();
            ViewBag.IdStatus = new SelectList(Business.StatusEvent.ListStatus().Select(x => new SelectListItem { Value = x.IdStatus.ToString(), Text = x.Name }).OrderBy(o => o.Text).ToList(), "Value", "Text", id.ToString());

            var model = new Models.EventModel();
            model.ListEvent = Business.Event.ListEvent(null, id);

            return View(model);
        }

        [SessionCheck]
        [HttpPost]
        public ActionResult Filtrar(Models.EventModel model)
        {
            var user = Geral.RecuperaCookieAdmin();

            ViewBag.Name = user.Name.ToString();
            ViewBag.Email = user.Email.ToString();
            ViewBag.IdStatus = new SelectList(Business.StatusEvent.ListStatus().Select(x => new SelectListItem { Value = x.IdStatus.ToString(), Text = x.Name }).OrderBy(o => o.Text).ToList(), "Value", "Text");

            model.ListEvent = Business.Event.ListEvent(model.Ibm, model.IdStatus);

            return View("Index", model);
        }

        [SessionCheck]
        [HttpGet]
        public ActionResult Export(long? Ibm, int? IdStatus)
        {
            ViewBag.IdStatus = new SelectList(Business.StatusEvent.ListStatus().Select(x => new SelectListItem { Value = x.IdStatus.ToString(), Text = x.Name }).OrderBy(o => o.Text).ToList(), "Value", "Text");

            var eventos = Business.Event.ListEvent(Ibm, IdStatus);
            // Create file
            DateTime now = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("Central Brazilian Standard Time"));
            string filename = "Relatorio_Eventos_" + now.ToString("ddMMyyyyHHmm") + ".xlsx";

            // Criar excel
            MemoryStream stream = new MemoryStream();
            ExcelStructure excel = new ExcelStructure("Eventos", ref stream);

            excel.setColumnWidth(1, 20);  // Ibm
            excel.setColumnWidth(2, 20);  // NameStatus
            excel.setColumnWidth(3, 20);  // Name
            excel.setColumnWidth(4, 30);  // Description
            excel.setColumnWidth(5, 20);  // DateEvent
            excel.setColumnWidth(6, 20);  // HourStart
            excel.setColumnWidth(7, 20);  // HourEnd
            excel.setColumnWidth(8, 20);  // Comments
            excel.setColumnWidth(9, 20);  // Reason
            excel.setColumnWidth(10, 20); // DateCreate
            excel.setColumnWidth(11, 20); // DateUpdate

            excel.createSheetData();

            IList<string> headers = new List<string>();
            IList<string> valores = new List<string>();
            IList<int> tipos = new List<int>();

            headers = new List<string>();

            headers.Add("Ibm");
            headers.Add("Status");
            headers.Add("Titulo");
            headers.Add("Descrição");
            headers.Add("Data Evento");
            headers.Add("Hora Inicio");
            headers.Add("Hora Termino");
            headers.Add("Observações");
            headers.Add("Motivo");
            headers.Add("Data Criação");
            headers.Add("Data Atualização");

            excel.CreateColumnHeader(1, headers);

            uint row = 2;
            foreach (var c in eventos)
            {
                valores = new List<string>();
                tipos = new List<int>();

                valores.Add(c.Ibm + " ");
                tipos.Add(ExcelStructure.TYPE_STRING);

                valores.Add(c.NameStatus + " ");
                tipos.Add(ExcelStructure.TYPE_STRING);

                valores.Add(c.Name + " ");
                tipos.Add(ExcelStructure.TYPE_STRING);

                valores.Add(c.Description + " ");
                tipos.Add(ExcelStructure.TYPE_STRING);

                valores.Add(c.DateEvent + " ");
                tipos.Add(ExcelStructure.TYPE_STRING);

                valores.Add(c.HourStart + " ");
                tipos.Add(ExcelStructure.TYPE_STRING);

                valores.Add(c.HourEnd + " ");
                tipos.Add(ExcelStructure.TYPE_STRING);

                valores.Add(c.Comments + " ");
                tipos.Add(ExcelStructure.TYPE_STRING);

                valores.Add(c.Reason + " ");
                tipos.Add(ExcelStructure.TYPE_STRING);

                valores.Add(c.DateCreate + " ");
                tipos.Add(ExcelStructure.TYPE_STRING);

                valores.Add(c.DateUpdate + " ");
                tipos.Add(ExcelStructure.TYPE_STRING);

                excel.CreateRowWithContent(row, valores, tipos);
                row++;
            }

            excel.save();
            excel.close();

            stream.Position = 0;

            return new FileStreamResult(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            {
                FileDownloadName = filename
            };
        }

        [SessionCheck]
        public ActionResult Details(int id)
        {
            var user = Geral.RecuperaCookieAdmin();

            ViewBag.Name = user.Name.ToString();
            ViewBag.Email = user.Email.ToString();

            var statusEvent = Business.StatusEvent.ListStatus();

            Event eventt = Business.Event.Details(id);
            Models.EventModel model = new Models.EventModel
            {
                IdEvent = eventt.IdEvent,
                Ibm = eventt.Ibm,
                IdStatus = eventt.IdStatus,
                Name = eventt.Name,
                Description = eventt.Description,
                Comments = eventt.Comments,
                DateEvent = eventt.DateEvent,
                HourStart = eventt.HourStart,
                HourEnd = eventt.HourEnd,
                Reason = eventt.Reason,
                DateCreate = eventt.DateCreate,
                DateUpdate = eventt.DateUpdate,
                NameStatus = eventt.NameStatus,
                ListStatusEvent = statusEvent
            };

            return View(model);
        }

        [SessionCheck]
        [HttpPost]
        public ActionResult Edit(string strDados)
        {
            Models.EventModel dados;

            try
            {
                dados = JsonConvert.DeserializeObject<Models.EventModel>(strDados);

                Business.Event.Edit(dados.IdEvent, (int)dados.IdStatus, dados.Name, dados.Description, dados.Comments, dados.Reason);
            }
            catch
            {

            }

            return Json(true);
        }
    }
}

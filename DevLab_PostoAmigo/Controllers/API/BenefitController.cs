﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DevLab_PostoAmigo.Data;
using DevLab_PostoAmigo.Business;
using System.Web.Http.Cors;

namespace DevLab_PostoAmigo.Controllers.API
{
    public class BenefitController : ApiController
    {
        // GET: api/Benefit?Ibm=5
        [EnableCors("*","*","*")]
        public dynamic Get(long Ibm)
        {
            return Business.Benfit.ListBenefit(Ibm);
        }

        // GET: api/Benefit/5
        [EnableCors("*", "*", "*")]
        public object Get(int id)
        {
            return Business.Benfit.GetBenefit(id);
        }

        // POST: api/Benefit
        [EnableCors("*", "*", "*")]
        public Business.Return.Model Post([FromBody]PostoAmigoBenefit benfit)
        {
            return Business.Benfit.Create(benfit);
        }

        // PUT: api/Benefit/5
        [EnableCors("*", "*", "*")]
        public Business.Return.Model Put(Business.Enum.PutBenfit id, [FromBody]PostoAmigoBenefit benfit)
        {
            if (id == Business.Enum.PutBenfit.Edit)
            {
                return Business.Benfit.Edit(benfit.IdBenefit, benfit.Description, benfit.DateStart, benfit.DateEnd);
            }
            else if (id == Business.Enum.PutBenfit.Extend)
            {
                return Business.Benfit.Extend(benfit.IdBenefit, benfit.DateEnd);
            }
            else {
                return new Business.Return.Model(400, false, "Ação desconhecida.");
            }
        }

        // DELETE: api/Benefit/5
        [EnableCors("*", "*", "*")]
        public Business.Return.Model Delete(int id)
        {
            return Business.Benfit.Cancel(id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DevLab_PostoAmigo.Data;
using DevLab_PostoAmigo.Business;
using System.Web.Http.Cors;

namespace DevLab_PostoAmigo.Controllers.API
{
    public class BenefitParticipantController : ApiController
    {
        // GET: api/BenefitParticipant/5
        [EnableCors("*", "*", "*")]
        public object Get(int id)
        {
            return Business.BenfitParticipant.GetQuantity(id);
        }

        // POST: api/BenefitParticipant
        [EnableCors("*", "*", "*")]
        public Business.Return.Model Post([FromBody]PostoAmigoBenefitParticipant participant)
        {
            return Business.BenfitParticipant.Create(participant);
        }
    }
}

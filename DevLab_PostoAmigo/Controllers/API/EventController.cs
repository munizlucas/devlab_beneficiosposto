﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DevLab_PostoAmigo.Data;
using DevLab_PostoAmigo.Business;
using System.Web.Http.Cors;

namespace DevLab_PostoAmigo.Controllers.API
{
    public class EventController : ApiController
    {
        // GET: api/Event?Ibm=5
        [EnableCors("*", "*", "*")]
        public dynamic Get(long Ibm)
        {
            return Business.Event.ListEvent(Ibm);
        }

        // GET: api/Event/5
        [EnableCors("*", "*", "*")]
        public object Get(int id)
        {
            return Business.Event.GetEvent(id);
        }

        // POST: api/Event
        [EnableCors("*", "*", "*")]
        public Business.Return.Model Post([FromBody]PostoAmigoEvent eevent)
        {
            return Business.Event.Create(eevent);
        }

        // PUT: api/Event/5
        [EnableCors("*", "*", "*")]
        public Business.Return.Model Put(Business.Enum.PutEvent id, [FromBody]PostoAmigoEvent eevent)
        {
            if (id == Business.Enum.PutEvent.Edit)
            {
                return Business.Event.Edit(eevent);
            }
            else {
                return new Business.Return.Model(400, false, "Ação desconhecida.");
            }
        }

        // DELETE: api/Benefit/5
        [EnableCors("*", "*", "*")]
        public Business.Return.Model Delete(int id)
        {
            return Business.Event.Cancel(id);
        }
    }
}

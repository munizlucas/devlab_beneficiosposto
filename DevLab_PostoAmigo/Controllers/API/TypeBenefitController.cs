﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DevLab_PostoAmigo.Data;
using DevLab_PostoAmigo.Business;
using System.Web.Http.Cors;

namespace DevLab_PostoAmigo.Controllers.API
{
    public class TypeBenefitController : ApiController
    {
        // GET: api/TypeBenefit
        [EnableCors("*", "*", "*")]
        public dynamic Get()
        {
            return Business.TypeBenefit.List();
        }
    }
}

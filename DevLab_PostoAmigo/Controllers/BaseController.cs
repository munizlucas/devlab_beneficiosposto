﻿using DevLab_PostoAmigo.Business.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DevLab_PostoAmigo.Controllers
{
    public class BaseController : Controller
    {
        public void Controller() {

            var user = Geral.RecuperaCookieAdmin();

            ViewBag.Name = user.Name.ToString();
            ViewBag.Email = user.Email.ToString();
        }
    }
}

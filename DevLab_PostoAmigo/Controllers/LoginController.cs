﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevLab_PostoAmigo.Models;
using DevLab_PostoAmigo.Data;
using DevLab_PostoAmigo.Business;
using System.Security.Claims;
using System.Web.Security;
using DevLab_PostoAmigo.Business.Util;

namespace DevLab_PostoAmigo.Controllers
{
    public class LoginController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Login";

            return View();
        }

        [HttpPost]
        public ActionResult Index(LoginModel model)
        {
            ViewBag.Title = "Login";

            var user = AdminLogin.Login(model.Login, model.Password);
            if (user == null)
            {
                ViewBag.Success = "Incorreto";
                return View(model);
            }
            else
            {
                FormsAuthentication.SetAuthCookie("Admin", false);
                Geral.CriaCookie(user);
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Geral.RemoveCookie("Admin");

            return RedirectToAction("Index", "Login");
        }
    }
}

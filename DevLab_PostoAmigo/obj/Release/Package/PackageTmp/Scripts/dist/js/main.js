function alerta(titulo, mensagem, reload, href) {
    $('#myModalLabel').text(titulo);
    $('#myModalTexto').html(mensagem);
    $('#myModal').modal('show');

    if (reload == true) {
        $('#myModal .fechar').on('click', function (event) {
            event.preventDefault();
            window.location.reload();
        });

    };

    if (href != undefined) {
        $('#myModal .fechar').on('click', function (event) {
            event.preventDefault();
            window.location.href = href
        });
    }
}

function letterCount(str) {
    var s = str.match(/([a-zA-Z])\1*/g) || [];
    return s.map(function (itm) {
        return [itm.charAt(0), itm.length];
    });
}

function validaEmail(e) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(e);
}

function validaCPF(cpf) {
    var Soma;
    var Resto;
    Soma = 0;
    if (cpf.length != 11 ||
        cpf == "00000000000" ||
        cpf == "11111111111" ||
        cpf == "22222222222" ||
        cpf == "33333333333" ||
        cpf == "44444444444" ||
        cpf == "55555555555" ||
        cpf == "66666666666" ||
        cpf == "77777777777" ||
        cpf == "88888888888" ||
        cpf == "99999999999")
        return false;

    for (i = 1; i <= 9; i++) Soma = Soma + parseInt(cpf.substring(i - 1, i)) * (11 - i);
    Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11)) Resto = 0;
    if (Resto != parseInt(cpf.substring(9, 10))) return false;

    Soma = 0;
    for (i = 1; i <= 10; i++) Soma = Soma + parseInt(cpf.substring(i - 1, i)) * (12 - i);
    Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11)) Resto = 0;
    if (Resto != parseInt(cpf.substring(10, 11))) return false;
    return true;
}

function validaData(value) {
    var d = new Date();
    d = parseInt(d.getUTCFullYear()) + ("0" + (d.getMonth() + 1)).slice(-2) + ("0" + d.getDate()).slice(-2)
    var date = value;
    var ardt = new Array;
    var ExpReg = new RegExp("(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/[12][0-9]{3}");
    ardt = date.split("/");
    var nS = ardt[2] + ardt[1] + ardt[0];

    erro = false;
    if (date == "") {
        erro == false;
    }
    else if (date.search(ExpReg) == -1) {
        erro = true;
    }
    else if (((ardt[1] == 4) || (ardt[1] == 6) || (ardt[1] == 9) || (ardt[1] == 11)) && (ardt[0] > 30)) {
        erro = true;
    }
    else if (ardt[1] == 2) {
        if ((ardt[0] > 28) && ((ardt[2] % 4) != 0))
            erro = true;
        if ((ardt[0] > 29) && ((ardt[2] % 4) == 0))
            erro = true;
    }
    else if (nS > d || nS < 19200101) {
        erro = true;
    } else if (ardt[2] < 1900)
        erro = true;

    if (erro) {
        return false;
    }
    return true;
}

function validaDataNascimento(valor) {
    var date = valor;
    var ardt = new Array;
    ardt = date.split("/");
    erro = false;
    if (!validaData(valor))
        erro = true;
    else if (ardt[2] > 2014)
        erro = true;

    if (erro)
        return false;
    else
        return true;
}

$(document).ready(function () {
    // #################################################################
    // #################################################################
    // #################################################################
    // ############										    ############
    // ############                 Geral                   ############
    // ############                                         ############
    // #################################################################
    // #################################################################
    // #################################################################

    $("a.em-breve").click(function () {
        alerta("Aviso", "Em breve.");
        return false;
    });
});
﻿using System.Web.Mvc;
using System.Web.Routing;
using DevLab_PostoAmigo.Business.Util;

namespace DevLab_PostoAmigo.Filters
{
    public class SessionCheckAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.Request.Cookies.Get("Admin") == null)
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { Controller = "Login", Action = "Logout" }));
            
        }
    }
}